"""Produce histogram of discriminant from tagger output and labels."""

import numpy as np
from ftag import Flavours

from puma import Histogram, HistogramPlot
from puma.utils import get_dummy_2_taggers, get_good_linestyles
import h5py
import pandas as pd

file_path = '../logs/HSGN2_20231001-T033947/ckpts/epoch=024-val_loss=0.28912__test_Hgamgam.h5'
with h5py.File(file_path, 'r') as hdf_file:
    ds = hdf_file['vertices']

    df = pd.DataFrame({'isHardScatter': np.array(ds['isHardScatter']).transpose(), 'HSGN2_phs': np.array(ds['HSGN2_phs']).transpose()})
    df = df.dropna()
    
    # defining boolean arrays to select the different flavour classes
    is_pu = df["isHardScatter"] == 0
    is_hs = df["isHardScatter"] == 1
    
    linestyles = get_good_linestyles()[:2]
    
    # Initialise histogram plot
    plot_histo = HistogramPlot(
        n_ratio_panels=0,
        ylabel="Normalised number of vertices",
        xlabel="HS discriminant",
        logy=True,
        leg_ncol=1,
        figsize=(5.5, 4.5),
        bins=np.linspace(0, 1, 100),
        y_scale=1.5,
        atlas_second_tag="$\\sqrt{s}=13.6$ TeV, ttbar",
    )
    
    # Add the histograms
    plot_histo.add(
        Histogram(
            df[is_pu]["HSGN2_phs"],
            label="PU vertices",
            colour=Flavours["bjets"].colour,
            linestyle=linestyles[0],
        ),
        reference=False,
    )
    plot_histo.add(
        Histogram(
            df[is_hs]["HSGN2_phs"],
            label="HS vertices",
            colour=Flavours["cjets"].colour,
            linestyle=linestyles[1],
        ),
        reference=False,
    )
    plot_histo.draw()
    plot_histo.savefig("histogram_discriminant.png", transparent=False)
