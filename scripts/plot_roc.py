"""Produce histogram of discriminant from tagger output and labels."""

import numpy as np
from ftag import Flavours

from puma import Roc, RocPlot
from puma import Histogram, HistogramPlot
from puma.metrics import calc_rej
from puma.utils import get_dummy_2_taggers, logger, get_good_linestyles
import h5py
import pandas as pd

file_paths_labels = [
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_Hgamgam.h5', 'Hyy'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_ttbar.h5', 'ttbar'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_JZ3.h5', 'JZ3'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_JZ7.h5','JZ7'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_Zmumu.h5','Zmumu'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_Ztautau.h5','Ztautau'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_eegamma.h5','eegamma'),
        ('../logs/HSGN2_20231001-T114854/ckpts/epoch=006-val_loss=0.25332__test_nunugamma.h5','nunugamma'),
]

for file_path, label in file_paths_labels:
    with h5py.File(file_path, 'r') as hdf_file:

        ds = hdf_file['vertices']

        df = pd.DataFrame({
            'isHardScatter': np.array(ds['isHardScatter']).transpose(), 
            'HSGN2_phs': np.array(ds['HSGN2_phs']).transpose(),
            'ntrk': np.array(ds['ntrk']).transpose(),
            'sumPt': np.array(ds['sumPt']).transpose(),
            'sumPt2': np.array(ds['sumPt2']).transpose(),
            'chi2Over_ndf': np.array(ds['chi2Over_ndf']).transpose(),
            'weighted_z_asymmetry': np.array(ds['weighted_z_asymmetry']).transpose(),
            'z_skewness': np.array(ds['z_skewness']).transpose(),
            'photon_deltaz': np.array(ds['photon_deltaz']).transpose()
        })
        df = df.dropna()

        # defining boolean arrays to select the different flavour classes
        is_pu = df["isHardScatter"] == 0
        is_hs = df["isHardScatter"] == 1

        # defining target efficiency
        sig_eff = np.linspace(0.75, 1, 100)

        n_pu = sum(is_pu)

        rej = calc_rej(df[is_hs]["HSGN2_phs"].values, df[is_pu]["HSGN2_phs"].values, sig_eff)

        # here the plotting of the roc starts
        plot_roc = RocPlot(
            n_ratio_panels=0,
            ylabel="PU rejection",
            xlabel="HS efficiency",
            atlas_second_tag=f"$\\sqrt{{s}}=13.6$ TeV, {label}",
            figsize=(6.5, 6),
            y_scale=1.4,
        )
        plot_roc.add_roc(
            Roc(
                sig_eff,
                rej,
                n_test=n_pu,
                rej_class="ujets",
                signal_class="bjets",
                label="HSGN2",
            ),
            reference=False,
        )

        plot_roc.draw()
        plot_roc.savefig(f"roc_{label}.png", transparent=False)

        linestyles = get_good_linestyles()[:2]
        
        # Initialise histogram plot
        plot_histo = HistogramPlot(
            n_ratio_panels=0,
            ylabel="Normalised number of vertices",
            xlabel="HS discriminant",
            logy=True,
            leg_ncol=1,
            figsize=(5.5, 4.5),
            bins=np.linspace(0, 1, 100),
            y_scale=1.5,
            atlas_second_tag=f"$\\sqrt{{s}}=13.6$ TeV, {label}",
        )
        
        # Add the histograms
        plot_histo.add(
            Histogram(
                df[is_pu]["HSGN2_phs"],
                label="PU vertices",
                colour=Flavours["bjets"].colour,
                linestyle=linestyles[0],
            ),
            reference=False,
        )
        plot_histo.add(
            Histogram(
                df[is_hs]["HSGN2_phs"],
                label="HS vertices",
                colour=Flavours["cjets"].colour,
                linestyle=linestyles[1],
            ),
            reference=False,
        )
        plot_histo.draw()
        plot_histo.savefig(f"histogram_discriminant_{label}.png", transparent=False)

        def plot_var(var, bins):
            # Initialise histogram plot
            plot_histo = HistogramPlot(
                n_ratio_panels=0,
                ylabel="Normalised number of vertices",
                xlabel=var,
                logy=False,
                leg_ncol=1,
                figsize=(5.5, 4.5),
                bins=np.linspace(*bins),
                y_scale=1.5,
                atlas_second_tag=f"$\\sqrt{{s}}=13.6$ TeV, {label}",
                underoverflow=False,
            )
            
            # Add the histograms
            plot_histo.add(
                Histogram(
                    df[is_pu][var],
                    label="PU vertices",
                    colour=Flavours["bjets"].colour,
                    linestyle=linestyles[0],
                ),
                reference=False,
            )
            plot_histo.add(
                Histogram(
                    df[is_hs][var],
                    label="HS vertices",
                    colour=Flavours["cjets"].colour,
                    linestyle=linestyles[1],
                ),
                reference=False,
            )
            plot_histo.draw()
            plot_histo.savefig(f"histogram_{var}_{label}.png", transparent=False)

        plot_var("ntrk",(0,250,125))
        plot_var("sumPt",(0,250000,125))
        plot_var("sumPt2",(0,2500,125))
        plot_var("chi2Over_ndf",(0,2,100))
        plot_var("weighted_z_asymmetry",(-1,1,100))
        plot_var("z_skewness",(-5,5,100))
        plot_var("photon_deltaz",(0,20,200))
