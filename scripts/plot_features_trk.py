"""Produce histogram of discriminant from tagger output and labels."""

import numpy as np
from ftag import Flavours

from puma import Histogram, HistogramPlot
from puma.utils import get_dummy_2_taggers, get_good_linestyles
import h5py
import pandas as pd

file_path = '/inputs/output/pp_output_train.h5'
with h5py.File(file_path, 'r') as hdf_file:

    df_v = pd.DataFrame(
        hdf_file["vertices"].fields(
            [
                "isHardScatter",
                "ntrk",
                "sumPt",
                "sumPt2",
                "chi2Over_ndf",
                "z_asymmetry",
                "weighted_z_asymmetry",
                "z_kurtosis",
                "z_skewness",
            ]
        )[:300000]
    )
    df_v = df_v.dropna()
    
    # defining boolean arrays to select the different flavour classes
    is_pu = df_v["isHardScatter"] == 0
    is_hs = df_v["isHardScatter"] == 1
    
    linestyles = get_good_linestyles()[:2]
    
    def plot_var_vertex(var, bins):
        # Initialise histogram plot
        plot_histo = HistogramPlot(
            n_ratio_panels=0,
            ylabel="Normalised number of vertices",
            xlabel=var,
            logy=False,
            leg_ncol=1,
            figsize=(5.5, 4.5),
            bins=np.linspace(*bins),
            y_scale=1.5,
            atlas_second_tag="$\\sqrt{s}=13.6$ TeV, ttbar",
        )
        
        # Add the histograms
        plot_histo.add(
            Histogram(
                df_v[is_pu][var],
                label="PU vertices",
                colour=Flavours["bjets"].colour,
                linestyle=linestyles[0],
            ),
            reference=False,
        )
        plot_histo.add(
            Histogram(
                df_v[is_hs][var],
                label="HS vertices",
                colour=Flavours["cjets"].colour,
                linestyle=linestyles[1],
            ),
            reference=False,
        )
        plot_histo.draw()
        plot_histo.savefig(f"histogram_{var}.png", transparent=False)

    def plot_var_track(var, index, bins):
        # Initialise histogram plot
        plot_histo = HistogramPlot(
            n_ratio_panels=0,
            ylabel="Normalised number of tracks",
            xlabel=var,
            logy=False,
            leg_ncol=1,
            figsize=(5.5, 4.5),
            bins=np.linspace(*bins),
            y_scale=1.5,
            atlas_second_tag="$\\sqrt{s}=13.6$ TeV, ttbar",
        )

        is_pu_mask = is_pu.astype(bool)
        is_hs_mask = is_hs.astype(bool)

        pu_arr = hdf_file["tracks"][:300000][var][is_pu_mask,:].flatten()
        hs_arr = hdf_file["tracks"][:300000][var][is_hs_mask,:].flatten()

        pu_arr = pu_arr[~np.isnan(pu_arr)]
        hs_arr = hs_arr[~np.isnan(hs_arr)]

        pu_arr = pu_arr[~(pu_arr == -1)]
        hs_arr = hs_arr[~(hs_arr == -1)]

        # Add the histograms
        plot_histo.add(
            Histogram(
                pu_arr,
                label="PU vertices",
                colour=Flavours["bjets"].colour,
                linestyle=linestyles[0],
            ),
            reference=False,
        )
        plot_histo.add(
            Histogram(
                hs_arr,
                label="HS vertices",
                colour=Flavours["cjets"].colour,
                linestyle=linestyles[1],
            ),
            reference=False,
        )
        plot_histo.draw()
        plot_histo.savefig(f"histogram_{var}.png", transparent=False)


    plot_var_vertex("ntrk",(0,250,125))
    plot_var_vertex("sumPt",(0,250000,125))
    plot_var_vertex("sumPt2",(0,2500,125))
    plot_var_vertex("chi2Over_ndf",(0,2,100))
    plot_var_vertex("z_asymmetry",(-1,1,100))
    plot_var_vertex("weighted_z_asymmetry",(-1,1,100))
    plot_var_vertex("z_kurtosis",(0,50,100))
    plot_var_vertex("z_skewness",(-5,5,100))

    plot_var_track("pt",0,(0,10000,100))
    plot_var_track("eta",1,(-2.5,2.5,100))
    plot_var_track("vertexWeight",3,(0,1,100))
    plot_var_track("isJet",6,(0,2.1,21))
    plot_var_track("isElectron",4,(0,2.1,21))
